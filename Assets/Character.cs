﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Character : MonoBehaviour {

	public AudioClip m_audioFishball;
	public GameObject m_objectTusokTip;

	public List<GameObject> m_listObjects;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Reset()
	{
		m_objectTusokTip.transform.localPosition = new Vector3(0, 0, 0);
		foreach (GameObject obj in m_listObjects) {
			obj.transform.parent = null;
		}
		m_listObjects.Clear ();
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Enemy" && GameScene.instance.m_eState != GAME_STATE.RESULTS){// && GameScene.instance.isThrusting ) {
			if (GameScene.instance.m_eState != GAME_STATE.SHIFT) {
				//coll.gameObject.SetActive (false);
				coll.gameObject.GetComponent<BoxCollider2D> ().enabled = false;
				coll.gameObject.GetComponent<Rigidbody2D> ().isKinematic = true;
				coll.gameObject.transform.parent = m_objectTusokTip.transform;
				LeanTween.moveLocal (coll.gameObject, new Vector3 (0, coll.gameObject.transform.localPosition.y - 2f, 0), 0.1f);
				LeanTween.rotateLocal (coll.gameObject, new Vector3 (0, 0, 0), 0.5f);
				m_objectTusokTip.transform.localPosition -= new Vector3 (0, 0.8f, 0);

				ZAudioMgr.Instance.PlaySFX (m_audioFishball);
				GameScene.instance.Score ();
				LeanTween.delayedCall (0.05f, Retract);

				//GameScene.instance.isStick = true;

				GameScene.instance.SpawnEnemy ();

				m_listObjects.Add (coll.gameObject);

				GameScene.instance.Hit (coll.gameObject.transform.position);
			}
		}
	}

	void Retract()
	{
		GameScene.instance.isThrusting = false;
		//GameScene.instance.isStick = false;
	}
}
