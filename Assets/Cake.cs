﻿using UnityEngine;
using System.Collections;

public class Cake : MonoBehaviour {

	public GameObject m_objectCakeAsset;
	public TextMesh m_textNumber;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (this.GetComponent<Rigidbody2D> ().isKinematic)
			return;
		this.GetComponent<Rigidbody2D> ().isKinematic = true;
		//GameScene.instance.CenterCamera (this.transform.position);
	}
}
