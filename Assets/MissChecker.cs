﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class MissChecker : MonoBehaviour
{
	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Arrow") {
			if (coll.gameObject.GetComponent<Rocket> ())
				coll.gameObject.GetComponent<Rigidbody2D> ().isKinematic = true;
			else
				coll.gameObject.SetActive (false);
			GameScene.instance.Die ();
		}
	}

	void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.tag == "Arrow") {
			if (coll.gameObject.GetComponent<Rocket> ())
				coll.GetComponent<Rigidbody2D> ().velocity = Vector3.zero;
			else
				coll.gameObject.SetActive (false);
		}
	}
}
