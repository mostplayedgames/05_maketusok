﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using UnityEngine.Analytics;
using GameAnalyticsSDK;

public class ZAnalytics : ZSingleton<ZAnalytics>  {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

	// IAP EVENTS
	public void SendBusinessEvent(string currency, int amount, string itemType, string itemID, string cartType)
	{
		#if UNITY_IOS
		GameAnalytics.NewBusinessEvent(currency, amount, itemType, itemID, cartType);
		#else
		GameAnalytics.NewBusinessEventGooglePlay(currency, amount, itemType, itemID, cartType, "", "");
		#endif
	}


	// EARN EVENTS
	// Implement on Shop
	public void SendSpendShopItemEvent(string itemId, float price)
	{
		GameAnalytics.NewResourceEvent (GAResourceFlowType.Sink, "coin", price, "shop", itemId);
	}

	// Implement on all areas with earn events, AddCoin
	public void SendEarnCoinsEvent(string itemId, float price)
	{
		GameAnalytics.NewResourceEvent (GAResourceFlowType.Source, "coin", price, "shop", itemId);
	}


	// PROGRESS EVENTS
	// Put on Success
	public void SendLevelComplete(string modeName, int levelNumber, int tries)
	{
		GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, modeName, "levelcompleted" + levelNumber, tries);
	}

	// Put on Setup Level
	public void SendLevelStart(string modeName, int levelNumber, int tries)
	{
		GameAnalytics.NewProgressionEvent (GAProgressionStatus.Start, modeName + "levelstarted" + levelNumber, tries);
	}

	// Put on Setup Level
	public void SendLevelFail(string modeName, int levelNumber, int currentScore)
	{
		GameAnalytics.NewProgressionEvent (GAProgressionStatus.Fail, modeName, "levelfailed" + levelNumber, currentScore);
	}

	// REWARD EVENTS
	public void SendLikeUsComplete()
	{
		GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "rewards", "likeuscomplete", 0);
	}

	public void SendFollowUsComplete()
	{
		GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "rewards", "followuscomplete", 0);
	}

	public void SendRateUsComplete()
	{
		GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "rewards", "rateuscomplete", 0);
	}

	public void SendMoreGamesComplete()
	{
		GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "rewards", "moregamescomplete", 0);
	}

	public void SendShareComplete()
	{
		GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "rewards", "sharecomplete", 0);
	}

	public void SendEarnComplete()
	{
		GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "rewards", "watchcomplete", 0);
	}

	public void SendLeaderboardsComplete()
	{
		GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "rewards", "leaderboardscomplete", 0);
	}

	public void SendShopComplete()
	{
		GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "rewards", "shopcomplete", 0);
	}


	// REWARD EVENTS
	public void SendLikeUsButton()
	{
		//GameAnalytics.NewDesignEvent ("likeusbutton", 0);
	}

	public void SendFollowUsButton()
	{
		//GameAnalytics.NewDesignEvent ("followusbutton", 0);
	}

	public void SendRateUsButton()
	{
		//GameAnalytics.NewDesignEvent ("rateusbutton", 0);
	}

	public void SendMoreGamesButton()
	{
		//GameAnalytics.NewDesignEvent ("moregamesbuttons", 0);
	}

	public void SendShareButton()
	{
		//GameAnalytics.NewDesignEvent ("sharebutton", 0);
	}

	public void SendLeaderboardsButton()
	{
		//GameAnalytics.NewDesignEvent ("leaderboardsbutton", 0);
	}

	public void SendShopButton()
	{
		//GameAnalytics.NewDesignEvent ("shopbutton", 0);
	}

	public void SendNoAdsButton()
	{
		//GameAnalytics.NewDesignEvent ("noadsbutton", 0);
	}

	public void SendRestoreIAPButton()
	{
		//GameAnalytics.NewDesignEvent ("restoreiapbutton", 0);
	}

	public void SendModesButton()
	{
		//GameAnalytics.NewDesignEvent ("modesbutton", 0);
	}


	// CUSTOM EVENTS
	public void SendCurrentCoins(int cash)
	{
		GameAnalytics.NewDesignEvent ("currentcoins", cash);
	}

	//public void SendScore(int score)
	//{
	//	GameAnalytics.NewDesignEvent ("scorepergame", score);
	//}

	public void SendUseChar(int characterId)
	{
		GameAnalytics.NewProgressionEvent (GAProgressionStatus.Complete, "charactershop", "character" + characterId, characterId);
	}

		//GameAnalytics.NewDesignEvent (string eventName, float eventValue);
		//GameAnalytics.NewBusinessEvent (string currency, int amount, string itemType, string itemId, string cartType, string receipt, bool autoFetchReceipt);
		/*int totalPotions = 5;
		int totalCoins = 100;
		string weaponID = "Weapon_102";
		Analytics.CustomEvent("gameOver", new Dictionary<string, object>
			{
				{ "potions", totalPotions },
				{ "coins", totalCoins },
				{ "activeWeapon", weaponID }
			});*/
		//}

	/*
	public void SendEvent()
	{
		//GameAnalytics.NewDesignEvent (string eventName, float eventValue);
		//GameAnalytics.NewBusinessEvent (string currency, int amount, string itemType, string itemId, string cartType, string receipt, bool autoFetchReceipt);
		/*int totalPotions = 5;
		int totalCoins = 100;
		string weaponID = "Weapon_102";
		Analytics.CustomEvent("gameOver", new Dictionary<string, object>
			{
				{ "potions", totalPotions },
				{ "coins", totalCoins },
				{ "activeWeapon", weaponID }
			});*/
	//}
		
	/*public void SendPurchase()
	{
		//GameAnalytics.NewBusinessEventIOS(string currency, int amount, string itemType, string itemId, string cartType, string receipt)
		//GameAnalytics.NewBusinessEventGooglePlay(string currency, int amount, string itemType, string itemId, string cartType, string receipt, string signature)
	}*/
}
