﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

//using ChartboostSDK;
using UnityEngine.Advertisements;

public enum GAME_STATE
{
	START,
	IDLE,
	AIMING,
	SHOOT,
	RESULTS,
	SHIFT,
}

public enum GAME_MODES
{
	CLASSIC,
	QUICK,
	ENDLESS,
	BAZOOKA
}

[System.Serializable]
public class ShopBird
{
	public GameObject objectImage;
	public Image imageButtons;
	public GameObject objectPrice;
	public Sprite spriteImage;
	public string name;
	public GameObject question;
}

[System.Serializable]
public class SFXHandler{
	public List<AudioClip> m_listAudio;
	public void PlaySFX(){
		int randomSFX = Random.Range (0, m_listAudio.Count-1);
		ZAudioMgr.Instance.PlaySFX (m_listAudio [randomSFX]);
	}
}

// iTunes - https://itunes.apple.com/us/app/make-pana-blue-eagle/id1119208392?mt=8
// https://play.google.com/store/apps/details?id=com.mostplayed.archereagle
// itms-apps://itunes.apple.com/app/idYOUR_APP_ID
// from iTunes PH : https://itunes.apple.com/ph/app/make-pana-blue-eagle/id1119208392?mt=8
// itms - itms-apps://itunes.apple.com/ph/app/make-pana-blue-eagle/id1119208392?mt=8

public class GameScene : MonoBehaviour 
{
	public static GameScene instance;

	public Character characterInstance;

	public GameObject m_rootSpawnLocation;

	public List<ShopBird> m_listShopBirds;

	public GameObject m_objectParticle;
	public GameObject m_objectParticleBlood;
	public GameObject m_objectParticleExplode;

	public GameObject m_prefabArrow;
	public GameObject m_prefabRocket;
	public GameObject m_prefabBird;
	public GameObject m_prefabBirdBig;

	public GameObject m_objectMiss;
	public GameObject m_objectSuccess;
	public GameObject m_objectTutorial;
	public GameObject m_objectTopBar;
	public GameObject m_objectHitParticle;

	public GameObject m_objectBest;

	public GameObject m_objectWatchVideo;
	public GameObject m_objectEarned;

	public GameObject m_objectNoInternet;
	public GameObject m_objectHeadshot;

	public GameObject m_objectReplay;
	public GameObject m_objectWatch;

	public GameObject m_objectStick;

	public GameObject m_prefabFishball;

	public GameObject m_objectTusokTip;
	public GameObject m_objectGuide;

	public GameObject m_objectNewBest;
	public GameObject m_objectBar;

	public GameObject m_splashImage;
	public GameObject m_rootUnlock;

	public GameObject m_objectShareUnlock;

	public GameObject m_objectGift;
	public GameObject m_objectUnlockPlayButton;

	public List<GameObject> m_listButtons;

	public GameObject m_rootResults;
	public GameObject m_rootUnlockObject;

	public GameObject m_fixedTopBar;
	public GameObject m_unlockBackgroundImage;
	public GameObject m_objectPanindaButton;

	public GameObject m_objectBackgroundObject;

	public SFXHandler m_sfxHit;

	public ZCameraMgr m_scriptCamera;

	public AudioClip m_audioShoot;
	public AudioClip m_audioDie;
	public AudioClip m_audioMiss;
	public AudioClip m_audioMiss1;
	public AudioClip m_audioMiss2;
	public AudioClip m_audioSuccess;
	public AudioClip m_audioBow;
	public AudioClip m_audioButton;
	public AudioClip m_audioHeadshot;
	public AudioClip m_audioSwoosh;
	public AudioClip m_audioWinSpacey;
	public AudioClip m_audioPowerup;

	public List<Sprite> m_listPlates;

	public Color m_colorTextCorrect;
	public Color m_colorTextMiss;
	public Color m_colorTextSuccess;

	public Camera m_cameraMain;
	public List<Color> m_listBackground;
	public List<Color> m_listBackgroundProgress;

	public Text m_textScore;
	public Text m_textLevel;
	public Text m_textHit;
	public Text m_textHit2;
	public Text m_textMiss;
	public Text m_textModes;
	public Text m_textModes2;
	public Text m_textTitle;
	public Text m_textPlateTitle;
	public Text m_textCharAvail;
	public Text m_textLevelNumber;
	public Text m_textPaninda;
	public Text m_textUnlockText;

	public Image m_imagePlate;

	public SpriteRenderer m_rendererBG;
	public List<Sprite> m_listBGSprites;

	public List<int> m_listPrices;

	public List<string> m_listModes;

	public SFXHandler m_sfxMiss;
	public SFXHandler m_sfxBomb;
	public AudioClip m_audioBazookaTakeoff;

	public GAME_STATE m_eState;

	public List<GameObject> m_listBirds;
	public List<int> m_listPurchaseOrder;

	Vector3 m_startPosition;
	Vector2 m_positionTouch;

	int m_currentLevel;
	int m_currentScore;
	int m_currentAds;
	int m_currentHits;
	int m_currentMode;
	int m_currentBird;
	int m_currentAttempts;
	int m_currentPurchaseCount;
	int m_currentRubberBandTries;

	float m_currentIncentifiedAdsTime;

	void Awake()
	{
		instance = this;

		Application.targetFrameRate = 60;
		if (!PlayerPrefs.HasKey ("classic")) {
			PlayerPrefs.SetInt ("classic", 0);
		}
		if (!PlayerPrefs.HasKey ("LevelQuick")) {
			PlayerPrefs.SetInt ("LevelQuick", 1);
		}
		if (!PlayerPrefs.HasKey ("LevelEndless")) {
			PlayerPrefs.SetInt ("LevelEndless", 1);
		}
		if (!PlayerPrefs.HasKey ("LevelBazooka")) {
			PlayerPrefs.SetInt ("LevelBazooka", 1);
		}

		if (!PlayerPrefs.HasKey ("Hit")) {
			PlayerPrefs.SetInt ("Hit", 0);
		}
		if (!PlayerPrefs.HasKey ("Mode")) {
			PlayerPrefs.SetInt ("Mode", 0);
		}
		if (!PlayerPrefs.HasKey ("Removeads")) {
			PlayerPrefs.SetInt ("Removeads", 0);
		}

		if (!PlayerPrefs.HasKey ("currentBird")) {
			PlayerPrefs.SetInt ("currentBird", 0);
		}

		if (!PlayerPrefs.HasKey ("attempts")) {
			PlayerPrefs.SetInt ("attempts", 0);
		}

		if (!PlayerPrefs.HasKey ("rubberbandtries")) {
			PlayerPrefs.SetInt ("rubberbandtries", 0);
			m_currentRubberBandTries = 0;
		}

		if (!PlayerPrefs.HasKey ("currentPurchaseCount")) {
			PlayerPrefs.SetInt ("currentPurchaseCount", 0);
		}

		for (int x = 0; x < 12; x++) {
			if (x == 0) {
				if (!PlayerPrefs.HasKey ("birdbought" + x)) {
					PlayerPrefs.SetInt ("birdbought" + x, 1);
				}
			} else {
				if (!PlayerPrefs.HasKey ("birdbought" + x)) {
					PlayerPrefs.SetInt ("birdbought" + x, 0);
				}
			}
		}

		/*if (!PlayerPrefs.HasKey ("Update2")) {
			PlayerPrefs.SetInt ("Update2", 0);
			PlayerPrefs.SetInt ("Mode", 3);

			m_currentPurchaseCount = 0;
			for (int x = 0; x < 9; x++) {
				if (x == 0) {
				} else {
					if (PlayerPrefs.GetInt ("birdbought" + x) > 0 ) {
						m_currentPurchaseCount++;
					}
				}
			}
			PlayerPrefs.SetInt ("currentPurchaseCount", m_currentPurchaseCount);
		}*/

		PlayerPrefs.Save ();

		ZAudioMgr.Instance.enabled = true;
		ZAdsMgr.Instance.enabled = true;
		ZIAPMgr.Instance.enabled = true;
		ZPlatformCenterMgr.Instance.enabled = true;
	}

	void Start()
	{
		//if( PlayerPrefs.GetInt ("Removeads") <= 0 )
		ZAdsMgr.Instance.ShowBanner (BANNER_TYPE.BOTTOM_BANNER);

		ZPlatformCenterMgr.Instance.Login ();

		ZObjectMgr.Instance.AddNewObject (m_prefabFishball.name, 50, "");
		//ZObjectMgr.Instance.AddNewObject (m_prefabRocket.name, 5, m_objectRootArrows);

		//ZObjectMgr.Instance.AddNewObject (m_prefabBird, 10, null);
		//ZObjectMgr.Instance.AddNewObject (m_prefabBirdBig, 5, null);

		m_currentPurchaseCount = PlayerPrefs.GetInt ("currentPurchaseCount");

		m_currentHits = PlayerPrefs.GetInt ("Hit");
		m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice() + " Tusoks";
		m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice() + " Tusoks";
		m_currentBird = PlayerPrefs.GetInt ("currentBird");

		m_currentMode = 0;// PlayerPrefs.GetInt ("Mode");
		m_currentAttempts = PlayerPrefs.GetInt ("attempts");

		m_splashImage.GetComponent<SpriteRenderer>().color = new Color(1,1,1,1);
		LeanTween.alpha (m_splashImage, 0, 0.4f);

		m_currentIncentifiedAdsTime = 60;
		m_objectWatchVideo.SetActive (false);
		m_objectNoInternet.SetActive (false);
		m_objectEarned.SetActive (false);
		m_objectShareUnlock.SetActive (false);

		m_textModes.text = m_listModes [m_currentMode] + " MODE";
		m_textModes2.text = m_listModes [m_currentMode] + " MODE";

		LoadData ();
		SetupLevel ();
		m_currentAds = 0;

		m_cameraMain.backgroundColor = Color.white;
		m_fixedTopBar.GetComponent<Image> ().color = Color.white;
	}

	public bool isThrusting = false;
	public bool isStick = false;

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (m_objectShop.activeSelf) {
				m_objectShop.SetActive (false);
				ZAudioMgr.Instance.PlaySFX (m_audioButton);
			}
			else
				Application.Quit ();
		}

		if (ZGameMgr.instance.isPause)
			return;

		switch (m_eState) {
		case GAME_STATE.START:

			break;
		case GAME_STATE.IDLE:
			if (Input.GetMouseButtonDown (0)) {
				ZAudioMgr.Instance.PlaySFX (m_audioSwoosh);
			}
			if (Input.GetMouseButtonDown (0) && !isThrusting) {// || Input.GetMouseButton (0)) {
				//m_startPosition = Input.mousePosition;
				/*m_eState = GAME_STATE.AIMING;
				if( m_currentMode == 3 )
					ZAudioMgr.Instance.PlaySFX (m_audioSwoosh);
				else
					ZAudioMgr.Instance.PlaySFX (m_audioBow);*/
				m_objectEarned.SetActive (false);
				//m_eState = GAME_STATE.AIMING;
				//LeanTween.cancel(m_objectStick.gameObject);
				//LeanTween.moveLocalY (m_objectStick.gameObject, 18f, 0.2f).setLoopPingPong ().setLoopCount (2);
				/*if (m_objectStick.transform.localPosition.y < 16.5f)
					m_objectStick.transform.localPosition += new Vector3 (0, 60, 0) * Time.deltaTime;
				else
					m_objectStick.transform.localPosition = new Vector3 (0, 16.6f, 0);*/

				isThrusting = true;
				//m_eState = GAME_STATE.AIMING;
			} /*else if (Input.GetMouseButtonUp (0) && isThrusting) {
				/*if (m_objectStick.transform.localPosition.y > 0.2f)
					m_objectStick.transform.localPosition -= new Vector3 (0, 40, 0) * Time.deltaTime;
				else
					m_objectStick.transform.localPosition = new Vector3 (0, 0, 0);*/

				//m_eState = GAME_STATE.SHOOT;

				//isThrusting = false;
			//}
			//if (!isStick) {
				if (isThrusting) {
					if (m_objectStick.transform.localPosition.y < 16.5f)
						m_objectStick.transform.localPosition += new Vector3 (0, 75, 0) * Time.deltaTime;
					else {
						m_objectStick.transform.localPosition = new Vector3 (0, 16.6f, 0);
						isThrusting = false;
					}
				} else {
					if (m_objectStick.transform.localPosition.y > 0.2f)
						m_objectStick.transform.localPosition -= new Vector3 (0, 40, 0) * Time.deltaTime;
					else
						m_objectStick.transform.localPosition = new Vector3 (0, 0, 0);
				}
			//}

			//m_eState = GAME_STATE.AIMING;

			break;
		case GAME_STATE.AIMING:
			/*Vector3 mouseDelta = Input.mousePosition - m_startPosition;
			if (mouseDelta.sqrMagnitude < 0.1f) {
				return; // don't do tiny rotations.
			}

			float angle = Mathf.Atan2 (Input.mousePosition.y, Input.mousePosition.x) * Mathf.Rad2Deg;
			if (angle < 0)
				angle += 360;*/

			/*Vector3 worldMousePosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			m_objectStick.transform.localPosition = new Vector3 (worldMousePosition.x, worldMousePosition.y + 7f, 0);
			*/
	
			if (Input.GetMouseButtonUp (0)) {
				//ShootArrow ();
				//m_eState = GAME_STATE.SHOOT;
				ZAudioMgr.Instance.PlaySFX (m_audioSwoosh);
				LeanTween.cancel(m_objectStick.gameObject);
				LeanTween.moveLocalY (m_objectStick.gameObject, 0, 0.5f);
				m_eState = GAME_STATE.IDLE;
			}
			break;
		}

		m_currentIncentifiedAdsTime -= Time.deltaTime;

	}

	void LoadData()
	{

	}

	public void Hit(Vector3 position)
	{
		if (m_eState == GAME_STATE.RESULTS)
			return;

		m_objectHitParticle.GetComponent<ParticleSystem> ().Clear ();
		m_objectHitParticle.transform.position = position;
		m_objectHitParticle.SetActive (true);
		m_objectHitParticle.GetComponent<ParticleSystem> ().Play ();

		//ZAudioMgr.Instance.PlaySFX (m_audioDie);
		m_sfxHit.PlaySFX();

		//UpdateBar ();

		m_eState = GAME_STATE.IDLE;

		//m_currentHits++;
	}

	public void HitExplode(Vector3 position)
	{
		if (m_eState == GAME_STATE.RESULTS)
			return;
		
		m_objectParticleExplode.GetComponent<ParticleSystem> ().Clear ();
		m_objectParticleExplode.transform.position = position;
		m_objectParticleExplode.SetActive (true);
		m_objectParticleExplode.GetComponent<ParticleSystem> ().Play ();

		//ZAudioMgr.Instance.PlaySFX (m_audioDie);
		m_sfxBomb.PlaySFX ();

		m_eState = GAME_STATE.IDLE;

		m_currentHits++;
	}

	public void HitHeadshot(Vector3 position)
	{
		m_objectParticleBlood.GetComponent<ParticleSystem> ().Clear ();
		m_objectParticleBlood.transform.position = position;
		m_objectParticleBlood.SetActive (true);
		m_objectParticleBlood.GetComponent<ParticleSystem> ().Play ();

		m_objectHeadshot.SetActive (true);
		LeanTween.scale (m_objectHeadshot, new Vector3(1.2f, 1.2f, 1.2f), 0.4f).setEase(LeanTweenType.easeInOutCubic).setLoopCount(2).setLoopPingPong().setOnComplete(HitHeadshotRoutine);

		ZAudioMgr.Instance.PlaySFX (m_audioHeadshot);
	}

	void HitHeadshotRoutine()
	{
		m_objectHeadshot.SetActive (false);
	}

	int rewardedSource = 0;
	public void Die()
	{
		if (m_eState == GAME_STATE.RESULTS)
			return;

		if (m_eState == GAME_STATE.SHIFT)
			return;

		StopCoroutine ("whileTest");
		m_textScore.color = m_colorTextMiss;
		//m_objectMiss.SetActive (true);
		m_objectGuide.SetActive (false);
		//m_textPlateTitle.gameObject.SetActive (true);
		m_textTitle.text = "";
		LeanTween.scale (m_textTitle.gameObject, new Vector3 (0.55f, 0.55f, 0.55f), 0.2f).setEase (LeanTweenType.easeInOutCubic).setLoopCount (2).setLoopPingPong ();

		m_textTitle.gameObject.SetActive (true);
		int chatvalue = Random.Range (0, 8);
		switch (chatvalue) {
		case 0 : 
			m_textTitle.text = "Hay Naku!";
			break;
		case 1 : 
			m_textTitle.text = "Ay Mintis!";
			break;
		case 2 : 
			m_textTitle.text = "Miss Kuya!";
			break;
		case 3 : 
			m_textTitle.text = "Para sa Ekonomiya!";
			break;
		default : 
			m_textTitle.text = "Miss!";
			break;
		}

		m_scriptCamera.DoShake ();

		ZAudioMgr.Instance.PlaySFX (m_audioDie);

		m_currentAttempts++;
		PlayerPrefs.SetInt ("attempts", m_currentAttempts);

		ZAnalytics.Instance.SendLevelFail ("mode" + m_currentMode, m_currentLevel, m_currentAttempts);
		m_textScore.text = "" + m_currentScore + " PCS";
		//LeanTween.moveLocal (m_objectStick, new Vector3(0, Mathf.Abs(characterInstance.m_objectTusokTip.transform.localPosition.y) + 2f, 0), 0.2f);


		/*
		if (m_currentAds > 3 && ZAdsMgr.Instance.IsInterstitialAvailable()) {
			ZAdsMgr.Instance.ShowInsterstitial ();
			m_currentAds = 0;
			LeanTween.delayedCall (1.75f, SetupLevel);
		}
		else if (m_currentHits >= GetCurrentShopPrice () && 
			m_currentPurchaseCount < m_listShopBirds.Count && 
			ZRewardsMgr.instance.WillShowShopButton()) {
			ZRewardsMgr.instance.ShowShopButton ();
			LeanTween.delayedCall (1.75f, SetupLevel);
		}
		else if (m_currentLevel > 2 && ZRewardsMgr.instance.WillShowReward ()) {
			LeanTween.delayedCall (1f, ShowReward);
		} else {
			LeanTween.delayedCall (1.75f, SetupLevel);
		}

		m_currentAds++;*/

		LeanTween.delayedCall (1.5f, ShowResultsScreen);
		Time.timeScale = 1f;

		m_sfxMiss.PlaySFX ();
		m_eState = GAME_STATE.RESULTS;

		foreach (GameObject obj in m_listBirds) {
			obj.GetComponent<Rigidbody2D> ().velocity = new Vector2 (-10f, 0);
		}

		PlayerPrefs.SetInt ("Hit", m_currentHits);
		m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice() + " Tusoks";
		m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice() + " Tusoks";

		if (m_currentMode == 0 ){
			if (m_currentScore > PlayerPrefs.GetInt ("classic")) {
				PlayerPrefs.SetInt ("classic", m_currentScore);
				m_currentLevel = m_currentScore;
				m_objectNewBest.SetActive (true);
				PlayerPrefs.SetInt ("rubberbandtries", 0);
				m_currentRubberBandTries = 0;
			} else {
				m_currentRubberBandTries = PlayerPrefs.GetInt ("rubberbandtries") + 1;
				PlayerPrefs.SetInt ("rubberbandtries", m_currentRubberBandTries);
			}
		}

		#if UNITY_ANDROID
		if (m_currentMode == 0) {
			ZPlatformCenterMgr.Instance.PostScore (m_currentScore, ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardAndroid);
		}
		#else
		if (m_currentMode == 0) {
		ZPlatformCenterMgr.Instance.PostScore (m_currentScore, ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardIOS);
		}
		#endif // UNITY_ANDROID
	}

	void UpdateBar()
	{
		float barScale = m_currentHits * 1.0f / GetCurrentShopPrice ();
		if (barScale >= 1)
			barScale = 1;
		m_objectBar.transform.localScale = new Vector3 (barScale, 1, 1);

		m_textLevelNumber.text = "" + (m_currentPurchaseCount+1);
	}

	void ShowReward(){
		//LeanTween.delayedCall (1.5f, SetupLevel);
		ZRewardsMgr.instance.ShowReward ();
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void DieForce()
	{
	}

	public void Score(int score = 1)
	{
		if (m_currentMode == 0) {
			m_currentScore += score;
			m_textScore.text = "" + m_currentScore + " PCS";
		}else {
			m_currentScore -= score;
			m_textScore.text = "" + m_currentScore + " PCS";
			if (m_currentScore <= 0) {
				Success ();
			} else {
				//LeanTween.delayedCall (0.5f, ScoreRoutine);
			}
				//LeanTween.delayedCall (0.5f, ScoreRoutine);
		}
		AddCoins (1);

		int randomColor = Mathf.FloorToInt(m_currentScore/10f);
		if (randomColor >= m_listBackgroundProgress.Count - 1) {
			randomColor = m_listBackgroundProgress.Count - 1;
		}
		//m_cameraMain.backgroundColor = m_listBackgroundProgress[randomColor];
		if (m_currentScore > 0 && m_currentScore % 10 == 0 && m_currentScore < m_listBackground.Count * 10) {
			LeanTween.color (m_objectBackgroundObject, m_listBackgroundProgress [randomColor], 0.3f);
			m_fixedTopBar.GetComponent<Image> ().color = m_listBackgroundProgress [randomColor];
			ZAudioMgr.Instance.PlaySFX (m_audioPowerup);
		}
	

		/*} else {
			if (m_currentScore <= 0)
				return;
		
			m_currentScore -= score;
			if (m_currentScore < 0)
				m_currentScore = 0;
		
			m_textScore.text = "" + m_currentScore;



			if (m_currentScore <= 0) {
				Success ();
			} else {
				LeanTween.delayedCall (0.5f, ScoreRoutine);
			}
		}*/
	}

	void Success()
	{
		if (m_eState == GAME_STATE.RESULTS)
			return;

		ZAnalytics.Instance.SendLevelComplete ("mode" + m_currentMode, m_currentLevel, m_currentAttempts);
		m_currentAttempts = 0;
		PlayerPrefs.SetInt ("attempts", m_currentAttempts);
		ZAnalytics.Instance.SendCurrentCoins (m_currentHits);

		m_textScore.color = m_colorTextSuccess;
		m_textLevel.color = m_colorTextSuccess;
		ZAudioMgr.Instance.PlaySFX (m_audioDie);
		m_objectSuccess.SetActive (true);
		m_objectParticle.SetActive (true);
		StopCoroutine ("whileTest");
		//LeanTween.delayedCall (3.5f, SuccessRoutine);
		LeanTween.delayedCall (3.5f, ShowResultsScreen);
		m_eState = GAME_STATE.RESULTS;

		//LeanTween.moveLocalY (m_objectHitBow, m_objectHitBow.transform.localPosition.y + 2, 0.5f).setLoopCount (6).setLoopPingPong();

		PlayerPrefs.SetInt ("Hit", m_currentHits);
		m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice() + " Tusoks";
		m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice() + " Tusoks";

		ZAudioMgr.Instance.PlaySFX (m_audioSuccess);


		int chatvalue = Random.Range (0, 7);
		switch (chatvalue) {
		case 0: 
			m_objectSuccess.GetComponent<Text>().text = "Rah rah rah!";
			break;
		case 1: 
			m_objectSuccess.GetComponent<Text>().text = "Green White Fight!";
			break;
		case 2: 
			m_objectSuccess.GetComponent<Text>().text = "Rektikano";
			break;
		case 3: 
			m_objectSuccess.GetComponent<Text>().text = "Derecho!";
			break;
		default: 
			m_objectSuccess.GetComponent<Text>().text = "Animo!";
			break;
		}

		if (m_currentMode == 1) {
			foreach (GameObject obj in m_listBirds) {
				obj.GetComponent<Rigidbody2D> ().velocity = new Vector2 (-10f, 0);
			}
		}

		#if UNITY_ANDROID
		/*if (m_currentMode == 0) {
			ZPlatformCenterMgr.Instance.PostScore ( PlayerPrefs.GetInt ("Level"), "CgkI85r1lMsYEAIQAQ");
		}
		else if (m_currentMode == 1) {
			ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt ("LevelQuick"), "CgkI85r1lMsYEAIQAg");
		}
		else if (m_currentMode == 2) {
			ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt ("LevelEndless"), "CgkI85r1lMsYEAIQAw");
		}
		else if (m_currentMode == 3) {
			// PUT NEW LB HERE
			ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt ("LevelBazooka"), "CgkI85r1lMsYEAIQCg");
		}*/
		//ZPlatformCenterMgr.Instance.PostScore(ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName, ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardAndroid);

		#else
		/*if (m_currentMode == 0) {
			ZPlatformCenterMgr.Instance.PostScore ( PlayerPrefs.GetInt ("Level"), "highscore_leaderboard");
		}
		else if (m_currentMode == 1) {
			ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt ("LevelQuick"), "highscore_leaderboard_quick");
		}
		else if (m_currentMode == 2) {
			ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt ("LevelEndless"), "highscore_leaderboard_endless");
		}
		else if (m_currentMode == 3) {
			ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt ("LevelBazooka"), "highscore_bazooka_leaderboard");
		}*/
		//ZPlatformCenterMgr.Instance.PostScore(ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName, ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardIOS);
		#endif // UNITY_ANDROID

		m_currentAds++;
	}

	public void ShowResultsScreen()
	{
		if( characterInstance.m_objectTusokTip.transform.localPosition.y >= -8.4f )
			LeanTween.moveLocal (m_objectStick, new Vector3(0, 11.9f, 0), 0.2f);
		else if( characterInstance.m_objectTusokTip.transform.localPosition.y >= -10.4f )
			LeanTween.moveLocal (m_objectStick, new Vector3(0, 13f, 0), 0.2f);
		else
			LeanTween.moveLocal (m_objectStick, new Vector3(0, 14.5f, 0), 0.2f);
		
		m_objectBest.SetActive (true);
		m_textTitle.gameObject.SetActive (true);
		LeanTween.scale (m_textTitle.gameObject, new Vector3 (0.55f, 0.55f, 0.55f), 0.2f).setEase (LeanTweenType.easeInOutCubic).setLoopCount (2).setLoopPingPong ();
		m_textMiss.text = "";

		int randomRange = Random.Range (0, 8);
		if (m_currentScore > 25) {
			switch (randomRange) {
			case 0:
				m_textTitle.text = "IMBA!";
				break;
			case 1:
				m_textTitle.text = "LAKAS!";
				break;
			case 2:
				m_textTitle.text = "YOUR THE BOSS!";
				break;
			case 3:
				m_textTitle.text = "THE BEST!";
				break;
			case 4:
				m_textTitle.text = "MISMO!";
				break;
			case 5:
				m_textTitle.text = "IDOL KITA!";
				break;
			case 6:
				m_textTitle.text = "MASTERCHEF KA SIR!";
				break;
			default:
				m_textTitle.text = "GG!";
				break;
			}
		} else {
			switch (randomRange) {
			case 0:
				m_textTitle.text = "LOVE KO TO";
				break;
			case 1:
				m_textTitle.text = "SO GOOD";
				break;
			case 2:
				m_textTitle.text = "GANERN SA SARAP";
				break;
			case 3:
				m_textTitle.text = "FISHBALLICIOUS";
				break;
			case 4:
				m_textTitle.text = "NASAYO NA ANG LAHAT";
				break;
			case 5:
				m_textTitle.text = "KEEP CALM AND MAKE TUSOK";
				break;
			case 6:
				m_textTitle.text = "BOOM PANES";
				break;
			default:
				m_textTitle.text = "SARAP";
				break;
			}
		}

		//SuccessRoutine2 ();
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		SetupPlate ();

		LeanTween.delayedCall (1.5f, ShowResultsScreen2);
	}

	void ShowResultsScreen2()
	{
		


		if (m_currentHits >= GetCurrentShopPrice () && 
			m_currentPurchaseCount < m_listShopBirds.Count - 1 && 
			ZRewardsMgr.instance.WillShowShopButton()) {

			//m_textTitle.gameObject.SetActive (false);
			m_objectMiss.SetActive (false);
			m_textCharAvail.gameObject.SetActive (true);
			ZRewardsMgr.instance.ShowShopButton ();
			//m_objectWatch.SetActive (true);
			//LeanTween.delayedCall (1.75f, SetupLevel);
			LeanTween.delayedCall (3f, ShowResultsScreen3);
			//ZAudioMgr.Instance.PlaySFX (m_audioButton);
			ZAudioMgr.Instance.PlaySFX (m_audioWinSpacey);

			LeanTween.scale (m_fixedTopBar, new Vector3 (1.1f, 1.1f, 1.1f), 0.2f).setEase (LeanTweenType.easeInOutCubic).setLoopCount (2).setLoopPingPong ();
			LeanTween.delayedCall (1.75f, ShowUnlock);

			m_unlockBackgroundImage.GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 0);
			LeanTween.delayedCall (1.6f, ShowUnlockBackgroundBG);


		}
		else if (m_currentAds > 2 && ZAdsMgr.Instance.IsInterstitialAvailable()) {
			ZAdsMgr.Instance.ShowInsterstitial ();
			m_currentAds = 0;
			//LeanTween.delayedCall (1.75f, SetupLevel);
			ShowResultsScreen3();
		}
		else if (ZRewardsMgr.instance.WillShowReward ()) {
			//LeanTween.delayedCall (1f, ShowReward);
			ShowReward();
			m_objectWatch.SetActive (true);
			LeanTween.delayedCall (1f, ShowResultsScreen3);
			ZAudioMgr.Instance.PlaySFX (m_audioWinSpacey);
		} else if (ZRewardsMgr.instance.WillShowRewardSuccess ()) {
			//LeanTween.delayedCall (1f, ShowReward);
			ZRewardsMgr.instance.ShowRewardSuccess ();
			m_objectWatch.SetActive (true);
			LeanTween.delayedCall (1f, ShowResultsScreen3);
			ZAudioMgr.Instance.PlaySFX (m_audioWinSpacey);
		}else {
			//LeanTween.delayedCall (1.75f, SetupLevel);
			ShowResultsScreen3();
		}

		m_currentAds++;




	}

	float goingLess = 0.3f;
	string m_currentUnlockedName = "";
	Sprite m_currentUnlockImage;

	public void UnlockBirdAnim()
	{
		BuyBirdFromScreen ();

		goingLess = 0.1f;
		//LeanTween.rotateZ (m_objectGift, 30f, goingLess).setEase (LeanTweenType.easeInOutCubic);//.setOnComplete(UnlockBirdAnim1);
		//LeanTween.scale (m_objectGift, new Vector3 (0f, 0f, 0f), 2f).setEase(LeanTweenType.easeOutCubic);
		m_objectGift.GetComponent<SpriteRenderer>().color = new Color(1,1,1,1);
		LeanTween.alpha(m_objectGift, 0, 0.75f);
		LeanTween.scale (m_rootUnlockObject, new Vector3 (2f, 2f, 2f), 1.5f).setOnComplete(UnlockBirdAnim2).setEase(LeanTweenType.easeOutExpo);
		m_rootUnlockObject.GetComponent<Image> ().sprite = m_currentUnlockImage;

		m_textPaninda.text = "";
		m_objectPanindaButton.SetActive (false);

		ZAudioMgr.Instance.PlaySFX (m_audioButton);


	}

	void UnlockBirdAnim2()
	{
		m_textPaninda.text = "UNLOCKED " + m_currentUnlockedName + "!";
		LeanTween.scale (m_textPaninda.gameObject, new Vector3 (0.7f, 0.7f, 0.7f), 0.2f).setEase (LeanTweenType.easeInOutCubic).setLoopCount (2).setLoopPingPong ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		LeanTween.delayedCall (3f, UnlockBirdAnim3);

		ZAudioMgr.Instance.PlaySFX (m_audioSuccess);
		ZAudioMgr.Instance.PlaySFX (m_audioWinSpacey);

		m_objectParticle.GetComponent<ParticleSystem> ().Clear ();
		m_objectParticle.SetActive (true);
		m_objectParticle.GetComponent<ParticleSystem> ().Play ();
	}
	
	void UnlockBirdAnim3()
	{
		//m_objectPanindaButton.SetActive (true);
		m_objectUnlockPlayButton.SetActive (true);
		LeanTween.scale (m_objectUnlockPlayButton, new Vector3 (1.2f, 1.2f, 1.2f), 0.2f).setEase (LeanTweenType.easeInOutCubic).setLoopCount (2).setLoopPingPong ();
		m_objectShareUnlock.SetActive (true);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void BuyBirdFromScreen()
	{
		int count = m_listPurchaseOrder[m_currentPurchaseCount];

		if (m_currentHits >= m_listPrices[m_currentPurchaseCount]) {
			m_currentHits -= m_listPrices[m_currentPurchaseCount];
			PlayerPrefs.SetInt ("Hit", m_currentHits);
			PlayerPrefs.SetInt ("birdbought" + count, 1);

			ZAnalytics.Instance.SendSpendShopItemEvent ("birdbought" + count, m_listPrices [m_currentPurchaseCount]);



			m_currentPurchaseCount++;
			PlayerPrefs.SetInt ("currentPurchaseCount", m_currentPurchaseCount);
			PlayerPrefs.Save ();

			m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice() + " Tusoks";
			m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice() + " Tusoks";
			//ZAudioMgr.Instance.PlaySFX (m_audioButton);
			//ShopScene.instance.SetupScene ();

			m_currentUnlockImage = m_listShopBirds [count].spriteImage;
			m_currentUnlockedName = m_listShopBirds [count].name;

			UpdateBar ();
		}
	}

	public void PlayFromUnlock()
	{
		m_rootUnlock.SetActive (false);
		LeanTween.alpha (m_unlockBackgroundImage, 0, 0.2f);
		//m_objectWatch.SetActive (false);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}
	/*void UnlockBirdAnim1()
	{
		if (goingLess <= 0) {
			PopUnlock ();
			return;
		}
		
		LeanTween.rotateZ (m_objectGift, -30f, goingLess).setEase (LeanTweenType.easeInOutCubic).setOnComplete(UnlockBirdAnim2);
		goingLess -= 0.01f;
	}

	void UnlockBirdAnim2()
	{
		if (goingLess <= 0) {
			PopUnlock ();
			return;
		}
		
		LeanTween.rotateZ (m_objectGift, 30f, goingLess).setEase (LeanTweenType.easeInOutCubic).setOnComplete(UnlockBirdAnim1);
		goingLess -= 0.01f;
	}

	void PopUnlock()
	{
		LeanTween.cancel (m_objectGift);
		m_objectGift.transform.localEulerAngles = Vector3.zero;
		LeanTween.scale (m_objectGift, new Vector3 (6f, 6f, 6f), 1f).setEase(LeanTweenType.easeOutCubic);
	}*/

	void ShowUnlockBackgroundBG()
	{
		LeanTween.alpha (m_unlockBackgroundImage, 1, 0.2f);
		m_textCharAvail.gameObject.SetActive (false);
	}

	void ShowUnlock()
	{
		m_textPaninda.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, -117);
		m_objectGift.GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
		m_rootUnlockObject.transform.localScale = Vector3.zero;

		m_objectShareUnlock.SetActive (false);
			
		m_objectGift.transform.localPosition = new Vector3 (0, -10, 0);
		m_rootUnlock.SetActive (true);
		m_objectGift.transform.localPosition += new Vector3 (0, 40f, 0);
		LeanTween.moveLocal(m_objectGift, m_objectGift.transform.localPosition - new Vector3 (0, 40f, 0), 0.5f).setEase(LeanTweenType.easeOutBounce);

		m_textUnlockText.text = "" + GetCurrentShopPrice () + " TUSOKS";
		m_objectUnlockPlayButton.SetActive (false);

		m_textPaninda.text = "UNLOCK PANINDA!";
		m_objectPanindaButton.SetActive (true);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		ZAudioMgr.Instance.PlaySFX (m_audioSwoosh);
	}

	void ShowResultsScreen3()
	{
		m_rootResults.SetActive (true);
		m_objectReplay.SetActive (true);
		m_objectTopBar.SetActive (true);
		m_textHit.gameObject.SetActive (true);
		m_objectMiss.SetActive (false);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		m_textModes.text = "BEST " + m_currentLevel;
		m_textModes2.text = "BEST " + m_currentLevel;


	}

	public void Replay()
	{
		if (m_eState == GAME_STATE.SHIFT)
			return;

		m_objectNewBest.SetActive (false);
		m_objectBest.SetActive (false);
		m_textTitle.gameObject.SetActive (false);
		m_rootResults.gameObject.SetActive (false);
		m_textScore.text = "";
		LeanTween.moveLocalY (m_objectStick, -6f, 0.4f).setOnComplete (ReplayRoutine);
		m_rootResults.SetActive (false);
		//m_textTitle.gameObject.SetActive (true);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		m_objectWatch.SetActive (false);
		m_objectEarned.SetActive (false);

		m_eState = GAME_STATE.SHIFT;

	}

	void ReplayRoutine()
	{
		characterInstance.Reset ();
		ZObjectMgr.Instance.ResetAll ();
		LeanTween.moveLocalY (m_objectStick, 0f, 0.3f).setOnComplete (ReplayRoutine2);

	}

	void ReplayRoutine2()
	{
		SetupLevel ();
	}

	public void ShowAdPopup(string title, string message)
	{	
		if (ZAdsMgr.Instance.removeAds > 0)
			return;

		if (!Advertisement.IsReady ("rewardedVideo"))
			return;

		MobileNativeDialog ratePopUp = new MobileNativeDialog(title, message);
		ratePopUp.OnComplete += OnShowAdPopUpClose;
	}
	private void OnShowAdPopUpClose(MNDialogResult result) {
		if (result == MNDialogResult.YES) {
			Advertisement.Show ("rewardedVideo");
			//m_objectTulong.SetActive (true);
			m_currentAds = 0;
		} else {
			m_currentAds = 2;
		}
	}

	public void ShowRewardedAdPopup(string title, string message)
	{	
		MobileNativeDialog ratePopUp = new MobileNativeDialog(title, message);
		ratePopUp.OnComplete += OnShowRewardedAdPopUpClose;
	}

	private void OnShowRewardedAdPopUpClose(MNDialogResult result) 
	{
		if (result == MNDialogResult.YES) 
		{
			ShowOptions options = new ShowOptions();
			options.resultCallback = AdCallbackhanler;
			Advertisement.Show ("rewardedVideo", options);
		}
	}

	void AdCallbackhanler(ShowResult result)
	{
		switch (result)
		{
		case ShowResult.Finished:
			if (rewardedSource == 0)
				ContinueLevel ();
			if (rewardedSource == 1) {
				AddCoins (100);
				//UpdateBar ();
				if (m_currentHits >= GetCurrentShopPrice () &&
					m_currentPurchaseCount < m_listShopBirds.Count - 1) {
					LeanTween.delayedCall (0.5f, ShowUnlock);
				}
				new MobileNativeMessage("You get 100 Tusoks", "Thank you for your Support!");
				//m_objectTulong.SetActive (true);
			}
			m_currentAds = 0;
			break;
		case ShowResult.Skipped:
			break;
		case ShowResult.Failed:
			break;
		}
	}

	void SuccessRoutine()
	{
		m_objectSuccess.SetActive (false);
		m_textLevel.text = "Level";
		m_textScore.text = "" + m_currentLevel;
		LeanTween.delayedCall (1f, SuccessRoutine2);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void SuccessRoutine2()
	{
		m_textScore.text = "" + (m_currentLevel+1);
		LeanTween.delayedCall (1f, SuccessRoutine3);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		if (m_currentHits >= GetCurrentShopPrice () && m_currentPurchaseCount < m_listShopBirds.Count) {
			ZRewardsMgr.instance.ShowShopButton ();
		}
		else if (m_currentLevel > 2 && ZRewardsMgr.instance.WillShowRewardSuccess ())
		{
			ZRewardsMgr.instance.ShowRewardSuccess ();
		}
	}

	void SuccessRoutine3()
	{
		m_objectSuccess.SetActive (false);
		m_currentLevel++;
		ResetScore();

		if( m_currentMode == 0 )
			PlayerPrefs.SetInt ("Level", m_currentLevel);
		else if( m_currentMode == 1 )
			PlayerPrefs.SetInt ("LevelQuick", m_currentLevel);
		else if( m_currentMode == 2 )
			PlayerPrefs.SetInt ("LevelEndless", m_currentLevel);
		else if( m_currentMode == 3 )
			PlayerPrefs.SetInt ("LevelBazooka", m_currentLevel);
		
		SetupLevel ();
	}

	void ScoreRoutine()
	{
		SpawnEnemy ();
	}

	void SpawnEnemyQuick()
	{
		if (m_eState == GAME_STATE.RESULTS)
			return;

		StartCoroutine("whileTest");
		SpawnEnemy ();
	}

	IEnumerator whileTest()
	{
		while(true == true)
		{
			float randomValue = 0f;
			if(m_currentLevel > 50)
				randomValue = Random.Range (5f, 6f);
			else if(m_currentLevel > 25)
				randomValue = Random.Range (5f, 6.5f);
			else
				randomValue = Random.Range (5f, 7f);

			randomValue = randomValue - (2.0f * ((m_currentLevel - m_currentScore) / m_currentLevel));
			yield return new WaitForSeconds (randomValue);
			SpawnEnemy ();
			Debug.Log ("while test working");
		}

	}

	float m_currentFishBall = 0;
	//int m_currentFishBallCount = 0;
	public void Play()
	{
		m_currentFishBall = Mathf.Min(0.2f, m_currentLevel / 300f);
		//Debug.LogError ("Current Fishball : " + m_currentFishBall + ", 2nd : " + (m_currentLevel/300f));
		//m_currentFishBallCount = 0;

		//m_textPlateTitle.gameObject.SetActive (false);

		LeanTween.delayedCall (0.75f, PlayRoutine);

		m_textScore.text = "" + m_currentScore + " PCS";

		m_textLevel.text = "";
		m_textScore.color = m_colorTextCorrect;
		//m_textHit.gameObject.SetActive (false);

		m_objectEarned.SetActive (false);
		m_objectNoInternet.SetActive (false);
		m_objectWatchVideo.SetActive (false);
		m_objectGuide.SetActive (true);
		//m_objectBest.SetActive (false);
		m_rootResults.gameObject.SetActive (false);

		m_objectTutorial.SetActive (false);
		m_objectTopBar.SetActive(false);


		foreach (GameObject btn in m_listButtons) {
			btn.gameObject.SetActive (false);
		}

		m_eState = GAME_STATE.IDLE;
	}

	void PlayRoutine()
	{
		if( m_currentMode == 0 || m_currentMode == 2 || m_currentMode == 3)
			SpawnEnemy ();
		if (m_currentMode == 1)
			SpawnEnemyQuick ();

		if (m_currentMode == 2) {
			m_currentScore = 0;
			m_textScore.text = "";// + m_currentScore;
		}

		//m_eState = GAME_STATE.IDLE;
	}

	public void ChangeMode()
	{
		m_currentMode++;
		if (m_currentMode >= m_listModes.Count) {
			m_currentMode = 0;
		}

		RefreshMode ();

		//m_textModes.text = m_listModes [m_currentMode] + " MODE";
		//m_textModes2.text = m_listModes [m_currentMode] + " MODE";

		PlayerPrefs.SetInt ("Mode", m_currentMode);

		ResetModeText ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void ResetModeText()
	{
		if (m_currentMode == 0)
			m_textLevel.text = "Level";
		else if (m_currentMode == 1)
			m_textLevel.text = "Level";
		else if (m_currentMode == 2)
			m_textLevel.text = "Highscore";
		else
			m_textLevel.text = "Level";
	}

	public void RemoveAds()
	{
		ZIAPMgr.Instance.PurchaseRemoveAds ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void FollowUs()
	{
		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.FOLLOWUS_URL_ANDROID);
		#else
		Application.OpenURL (ZGameMgr.instance.FOLLOWUS_URL_IOS);
		#endif

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public string ScreenshotName = "archereagle_screenshot.png";
	string m_screenshotText;
	string m_screenshotPath;

	public void ShareScreenshotWithText(string text)
	{
		m_screenshotText = text;
		m_screenshotPath = Application.persistentDataPath + "/" + ScreenshotName;
		Application.CaptureScreenshot(ScreenshotName);

		StartCoroutine("ScreenshotWriteCheck");
	}

	IEnumerator ScreenshotWriteCheck()
	{
		while (true == true) {
			if (System.IO.File.Exists(m_screenshotPath)) {
				ShareScreenshotRoutine ();
				yield return null;
			} else {
				yield return new WaitForSeconds (0.5f);
			}
		}
	}

	void ShareScreenshotRoutine ()
	{
		#if UNITY_ANDROID
		StartCoroutine(AndroidShare (m_screenshotText, m_screenshotPath, "", ZGameMgr.instance.GAME_HASHTAG));
		#else
		Share (m_screenshotText, m_screenshotPath, "", ZGameMgr.instance.GAME_HASHTAG);
		#endif 
		StopCoroutine("ScreenshotWriteCheck");

	}

	private IEnumerator AndroidShare(string shareText, string imagePath, string url, string subject = "")
	{
		AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
		AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + imagePath);
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
		intentObject.Call<AndroidJavaObject>("setType", "image/png");
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText);

		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

		AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, subject);
		currentActivity.Call("startActivity", jChooser);

		return null;
	}

	public void Share(string shareText, string imagePath, string url, string subject = "")
	{
		#if UNITY_ANDROID
		/*AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
		AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + imagePath);
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
		intentObject.Call<AndroidJavaObject>("setType", "image/png");

		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText);

		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

		AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, subject);
		currentActivity.Call("startActivity", jChooser);*/
		#elif UNITY_IOS
		CallSocialShareAdvanced(shareText, subject, url, imagePath);
		#else
		Debug.Log("No sharing set up for this platform.");
		#endif
	}

	#if UNITY_IOS
	public struct ConfigStruct
	{
		public string title;
		public string message;
	}

	[DllImport ("__Internal")] private static extern void showAlertMessage(ref ConfigStruct conf);

	public struct SocialSharingStruct
	{
		public string text;
		public string url;
		public string image;
		public string subject;
	}

	[DllImport ("__Internal")] private static extern void showSocialSharing(ref SocialSharingStruct conf);

	public static void CallSocialShare(string title, string message)
	{
		ConfigStruct conf = new ConfigStruct();
		conf.title  = title;
		conf.message = message;
		showAlertMessage(ref conf);
	}

	public static void CallSocialShareAdvanced(string defaultTxt, string subject, string url, string img)
	{
		SocialSharingStruct conf = new SocialSharingStruct();
		conf.text = defaultTxt; 
		conf.url = url;
		conf.image = img;
		conf.subject = subject;

		showSocialSharing(ref conf);
	}
	#endif

	public void SharePhoto()
	{
		ShareScreenshotWithText ("");
	}

	public void OpenLeaderboard()
	{
		#if UNITY_ANDROID
		/*if (m_currentMode == 0) {
			ZPlatformCenterMgr.Instance.ShowLeaderboardUI("CgkI85r1lMsYEAIQAQ");
		}
		else if (m_currentMode == 1) {
			ZPlatformCenterMgr.Instance.ShowLeaderboardUI("CgkI85r1lMsYEAIQAg");
		}
		else if (m_currentMode == 2) {
			ZPlatformCenterMgr.Instance.ShowLeaderboardUI("CgkI85r1lMsYEAIQAw");
		}
		else if (m_currentMode == 3) {
			ZPlatformCenterMgr.Instance.ShowLeaderboardUI("CgkI85r1lMsYEAIQCg");
		}*/
		ZPlatformCenterMgr.Instance.ShowLeaderboardUI(ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardAndroid);
		#else
		ZPlatformCenterMgr.Instance.ShowLeaderboardUI();
		#endif // UNITY_ANDROID
	
	}

	public void LikeUs()
	{
		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.LIKEUS_URL_ANDROID);
		#else
		Application.OpenURL (ZGameMgr.instance.LIKEUS_URL_IOS);
		#endif

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public GameObject m_objectShop;

	public void ShowShop()
	{
		m_objectShop.SetActive(true);
		ShopScene.instance.SetupScene();
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void RateUs()
	{
		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.RATEUS_URL_ANDROID);
		#else
		MNIOSNative.RedirectToAppStoreRatingPage(ZGameMgr.instance.RATEUS_URL_IOS);
		#endif
	}

	public void BuyCoins()
	{
		ZIAPMgr.Instance.PurchaseCoins ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void AddCoins(int index)
	{
		Debug.Log ("Add coins : " + index);
		m_currentHits += index;
		PlayerPrefs.SetInt ("Hit", m_currentHits);
	
		m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice() + " Tusoks";
		m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice() + " Tusoks";

		//if (m_currentHits >= GetCurrentShopPrice () && m_eState == GAME_STATE.START && m_currentPurchaseCount < m_listShopBirds.Count) {
		//	LeanTween.delayedCall (0.2f, AddCoinRoutine);
		//}

		if (m_currentHits >= GetCurrentShopPrice () &&
			m_currentPurchaseCount < m_listShopBirds.Count - 1 &&
			index > 20 ) {
			LeanTween.scale (m_fixedTopBar, new Vector3 (1.1f, 1.1f, 1.1f), 0.2f).setEase (LeanTweenType.easeInOutCubic).setLoopCount (2).setLoopPingPong ();
			LeanTween.delayedCall (1.75f, ShowUnlock);

			m_unlockBackgroundImage.GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 0);
			LeanTween.delayedCall (1.6f, ShowUnlockBackgroundBG);
		}

		UpdateBar ();
	}

	public void RestoreAds()
	{
		ZIAPMgr.Instance.RestorePurchases ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void MoreGames()
	{
		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.MOREGAMES_URL_ANDROID);
		#else
		Application.OpenURL (ZGameMgr.instance.MOREGAMES_URL_IOS);
		#endif

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void SpawnViaMode()
	{
		switch(m_currentMode)
		{
		case 0:
			break;
		case 1: 
			break;
		case 2:
			break;
		}

		//m_objectHeadshot.SetActive (false);
	}

	public void UseBird(int count)
	{
		if (PlayerPrefs.GetInt ("birdbought" + count) > 0) {
			m_currentBird = count;
			m_objectShop.SetActive (false);
			PlayerPrefs.SetInt ("currentBird", m_currentBird);
			ZAudioMgr.Instance.PlaySFX (m_audioButton);
			ZAnalytics.Instance.SendUseChar (m_currentBird);
		} else {
			BuyBird (count);
			ZAudioMgr.Instance.PlaySFX (m_audioButton);
		}
	}

	public int GetCurrentShopPrice()
	{
		return m_listPrices [m_currentPurchaseCount];
	}

	public void BuyBird(int count)
	{
		if (m_currentHits >= m_listPrices[m_currentPurchaseCount]) {
			m_currentHits -= m_listPrices[m_currentPurchaseCount];
			PlayerPrefs.SetInt ("Hit", m_currentHits);
			PlayerPrefs.SetInt ("birdbought" + count, 1);

			ZAnalytics.Instance.SendSpendShopItemEvent ("birdbought" + count, m_listPrices [m_currentPurchaseCount]);

			

			m_currentPurchaseCount++;
			PlayerPrefs.SetInt ("currentPurchaseCount", m_currentPurchaseCount);
			PlayerPrefs.Save ();

		m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice() + " Tusoks";
		m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice() + " Tusoks";
			//ZAudioMgr.Instance.PlaySFX (m_audioButton);
			ShopScene.instance.SetupScene ();

			UpdateBar ();
		}
	}
	
	float m_currentX = 0;
	int m_currentSortingLayerID = 0;

	public void SpawnEnemy()
	{
		if (m_eState == GAME_STATE.RESULTS)
			return;


		float randomValue = 1.2f;//Random.Range (0.7f, 1.2f);
	
		//+ STATS Varied
		float HORIZONTAL_SPEED_DEFAULT = 1;
		float GRAVITY_DEFAULT = 1;
		float TORQUE_DEFAULT = 1;

		float horizontalSpeed = 1;
		float gravityValue = 1;
		float torqueValue = 1;
		float spawnTimeDiff = 0;

		//- STATS Varied

		GameObject obj;
		Vector3 spawnLocation = m_rootSpawnLocation.transform.position + new Vector3(Random.Range(-6f,3f), 0, 0);
		if( m_currentX < -2.5f && spawnLocation.x > 1.5f )
			obj = ZObjectMgr.Instance.Spawn2D (m_prefabFishball.name, m_rootSpawnLocation.transform.position);
		else
			obj = ZObjectMgr.Instance.Spawn2D (m_prefabFishball.name, spawnLocation);

		obj.transform.parent = null;
		if (m_currentX < -2.5f && spawnLocation.x > 1.5f) {
			obj.transform.localPosition =  m_rootSpawnLocation.transform.position;
		} else {
			obj.transform.localPosition = spawnLocation;
		}

		//obj.GetComponent<Rigidbody2D> ().gravityScale = Random.Range (0.8f, 1.2f);
		obj.GetComponent<Rigidbody2D> ().gravityScale = Random.Range (0.8f, 2.5f);
		obj.GetComponent<Rigidbody2D> ().isKinematic = false;
		obj.GetComponent<BoxCollider2D> ().enabled = true;
		//obj.GetComponent<Rigidbody2D> ().gravityScale = 1.2f;

		obj.GetComponent<SpriteRenderer> ().sortingOrder = m_currentSortingLayerID;
		m_currentSortingLayerID++;

		m_currentX = spawnLocation.x;



		if (m_currentLevel > 60) {
		} else if (m_currentLevel > 50) {
		} else if (m_currentLevel > 40) {
		} else if (m_currentLevel > 30) {
		} else if (m_currentLevel > 20) {
		} else if (m_currentLevel > 10) {
		} else if (m_currentLevel > 5) {
		} else  {
		}



		int randomSpeed = Random.Range (0, 100);

		float rubberbandEasier = 0;
		rubberbandEasier = m_currentRubberBandTries / 5f;
		if (rubberbandEasier > 5) {
			rubberbandEasier = 5;
		}

		float LEVEL_1_VALUE = Mathf.Max(5f - ((m_currentLevel * 1.0f)/5f) + rubberbandEasier, 3f);
		float LEVEL_2_VALUE = Mathf.Max(7f - ((m_currentLevel * 1.0f)/5f) + rubberbandEasier, 5f);
		float LEVEL_3_VALUE = Mathf.Max(10f - ((m_currentLevel * 1.0f)/10f) + rubberbandEasier, 5f);
		float LEVEL_4_VALUE = Mathf.Max(20f - ((m_currentLevel * 1.0f)/10f) + rubberbandEasier, 10f);
		float LEVEL_5_VALUE = Mathf.Max(30f - ((m_currentLevel * 1.0f)/10f) + rubberbandEasier, 20f);
		float LEVEL_6_VALUE = Mathf.Max(40f - ((m_currentLevel * 1.0f)/10f) + rubberbandEasier, 30f);

		if (m_currentLevel <= 5f) {
			LEVEL_1_VALUE = 5f;
		} else if (m_currentLevel <= 7f) {
			LEVEL_2_VALUE = 7f;
		} else if (m_currentLevel <= 10f) {
			LEVEL_3_VALUE = 10f;
		} else if (m_currentLevel <= 20f) {
			LEVEL_4_VALUE = 20f;
		} else if (m_currentLevel <= 30f) {
			LEVEL_5_VALUE = 30f;
		} else if (m_currentLevel <= 40f) {
			LEVEL_6_VALUE = 40f;
		} 

		/*if (m_currentScore > 60) {
			Time.timeScale = 1.3f;
		} else if (m_currentScore > 50) {
			Time.timeScale = 1.25f;
		} else */if (m_currentScore >= LEVEL_6_VALUE ) {
			//Time.timeScale = 1.1f;

			int randomRangeSpeed = Random.Range (0, 100);

			// RIGHT SPAWN
			if (randomRangeSpeed > 66) {
				obj.transform.localPosition = m_rootSpawnLocation.transform.position + new Vector3 (6f, -2f);

				if (randomSpeed > 75) {
					obj.GetComponent<Rigidbody2D> ().AddForce (new Vector3(-350f, 400f));
				} else if (randomSpeed > 50) {
					obj.GetComponent<Rigidbody2D> ().AddForce (new Vector3(-175f, 400f));
				} else {
					obj.GetComponent<Rigidbody2D> ().AddForce (new Vector3 (Random.Range(-175f, -350f), 400f));
				}
				obj.GetComponent<Rigidbody2D> ().gravityScale = 2f;
			}
			// LEFT
			else if (randomRangeSpeed > 33) {
				obj.transform.localPosition = m_rootSpawnLocation.transform.position + new Vector3 (-13f, -6f);
	
				if (randomSpeed > 75) {
					obj.GetComponent<Rigidbody2D> ().AddForce (new Vector3(190f, 500f));
				} else if (randomSpeed > 50) {
					obj.GetComponent<Rigidbody2D> ().AddForce (new Vector3(700f, 500f));
				} else {
					obj.GetComponent<Rigidbody2D> ().AddForce (new Vector3 (Random.Range(190f, 700f), 500f));
				}

				obj.GetComponent<Rigidbody2D> ().gravityScale = 2f;
			} // TOP 
			else {
				if (obj.transform.localPosition.x > -1f && obj.transform.localPosition.x < 1f) {
					if (randomSpeed > 75) {
						obj.GetComponent<Rigidbody2D> ().AddForce (new Vector3 (250f, 0));
					} else if (randomSpeed > 50) {
						obj.GetComponent<Rigidbody2D> ().AddForce (new Vector3 (-100f, 0));
					} else {
						obj.GetComponent<Rigidbody2D> ().AddForce (new Vector3 (Random.Range (-100f, 250f), 0));
					}
				} else if (obj.transform.localPosition.x > -1.75f && obj.transform.localPosition.x < 3f) {
					obj.GetComponent<Rigidbody2D> ().AddForce (new Vector3 (Random.Range (-100f, 100f), 0));
				} 
				//obj.GetComponent<Rigidbody2D> ().gravityScale = 1.3f;
			}
			

			//LeanTween.delayedCall (1.2f, SpawnEnemy);
			randomValue = 1.7f - Mathf.Min (0.5f, m_currentFishBall * 2);
			//LeanTween.delayedCall (randomValue, SpawnEnemy);

			obj.transform.localEulerAngles = new Vector3 (0, 0, Random.Range (0, 270));
			if (Random.Range (0, 100) > 50)
				obj.GetComponent<Rigidbody2D> ().AddTorque (-120f);
			else
				obj.GetComponent<Rigidbody2D> ().AddTorque (120f);
		} else if (m_currentScore > LEVEL_5_VALUE) {
			//Time.timeScale = 1.15f;
			obj.transform.localPosition = m_rootSpawnLocation.transform.position + new Vector3 (6f, -2f);
			// min -250f, 650f
		// max -150f, 650f
		//obj.GetComponent<Rigidbody2D> ().AddForce (new Vector3(-250f, 650f));
			//obj.GetComponent<Rigidbody2D> ().AddForce (new Vector3(-150f, 650f));
		// -400
			if (randomSpeed > 75) {
				obj.GetComponent<Rigidbody2D> ().AddForce (new Vector3(-350f, 400f));
			} else if (randomSpeed > 50) {
				obj.GetComponent<Rigidbody2D> ().AddForce (new Vector3(-175f, 400f));
			} else {
				obj.GetComponent<Rigidbody2D> ().AddForce (new Vector3 (Random.Range(-175f, -350f), 400f));
			}
			obj.GetComponent<Rigidbody2D> ().gravityScale = 2f;

			//LeanTween.delayedCall (1.2f, SpawnEnemy);
			randomValue = 1.4f - Mathf.Min (0.5f, m_currentFishBall * 2);
			//LeanTween.delayedCall (randomValue, SpawnEnemy);

			obj.transform.localEulerAngles = new Vector3 (0, 0, Random.Range (0, 270));
			if (Random.Range (0, 100) > 50)
				obj.GetComponent<Rigidbody2D> ().AddTorque (-100f);
			else
				obj.GetComponent<Rigidbody2D> ().AddTorque (100f);

		} else if (m_currentScore > LEVEL_4_VALUE) {
			//Time.timeScale = 1.1f;
			obj.transform.localPosition = m_rootSpawnLocation.transform.position + new Vector3 (-13f, -6f);
		// min 150f, 250f
		// max 400f, 400f
		// 700, 500
			if (randomSpeed > 75) {
				obj.GetComponent<Rigidbody2D> ().AddForce (new Vector3(190f, 500f));
			} else if (randomSpeed > 50) {
				obj.GetComponent<Rigidbody2D> ().AddForce (new Vector3(700f, 500f));
			} else {
				obj.GetComponent<Rigidbody2D> ().AddForce (new Vector3 (Random.Range(190f, 700f), 500f));
			}

			obj.GetComponent<Rigidbody2D> ().gravityScale = 2f;

			randomValue = 1.4f - Mathf.Min (0.5f, m_currentFishBall * 2);
			//LeanTween.delayedCall (randomValue, SpawnEnemy);

			obj.transform.localEulerAngles = new Vector3 (0, 0, Random.Range (0, 270));
			if (Random.Range (0, 100) > 50)
				obj.GetComponent<Rigidbody2D> ().AddTorque (-75f);
			else
				obj.GetComponent<Rigidbody2D> ().AddTorque (75f);
			//obj.GetComponent<Rigidbody2D> ().gravityScale = Random.Range (0.8f, 1.5f);
		} else if (m_currentScore > LEVEL_3_VALUE) {
			//Time.timeScale = 1.05f;
			if (obj.transform.localPosition.x > -1f && obj.transform.localPosition.x < 1f) {
				// max x 250f // min x 100f
				if (randomSpeed > 50) {
					obj.GetComponent<Rigidbody2D> ().AddForce (new Vector3 (Random.Range (220f,110f), 0));
				} else{
					obj.GetComponent<Rigidbody2D> ().AddForce (new Vector3 (Random.Range (-100f, -50f), 0));
				} 
			} else if (obj.transform.localPosition.x > -1.75f && obj.transform.localPosition.x < 3f) {
				obj.GetComponent<Rigidbody2D> ().AddForce (new Vector3 (Random.Range (-100f, 100f), 0));
			} 

			//obj.GetComponent<Rigidbody2D> ().gravityScale = Random.Range (0.8f, 1.5f);
			//Time.timeScale = 1;

			//randomValue = randomValue - Mathf.Min (1f, m_currentFishBall);
			//LeanTween.delayedCall (1f, SpawnEnemy);

			obj.transform.localEulerAngles = new Vector3 (0, 0, Random.Range (0, 270));
			if (Random.Range (0, 100) > 50)
				obj.GetComponent<Rigidbody2D> ().AddTorque (-50f);
			else
				obj.GetComponent<Rigidbody2D> ().AddTorque (50f);
		} else if (m_currentScore > LEVEL_2_VALUE) {
	
			randomValue = randomValue - Mathf.Min (0.5f, m_currentFishBall * 2);
			//LeanTween.delayedCall (randomValue, SpawnEnemy);

			obj.transform.localEulerAngles = new Vector3 (0, 0, Random.Range (0, 270));
			if (Random.Range (0, 100) > 50)
				obj.GetComponent<Rigidbody2D> ().AddTorque (-25f);
			else
				obj.GetComponent<Rigidbody2D> ().AddTorque (25f);
		} else if (m_currentScore > LEVEL_1_VALUE) {
			//obj.GetComponent<Rigidbody2D> ().gravityScale = Random.Range (0.8f, 1.5f);
			//Time.timeScale = 1;

			randomValue = randomValue - Mathf.Min (0.5f, m_currentFishBall * 2);
			//LeanTween.delayedCall (randomValue, SpawnEnemy);
			obj.GetComponent<Rigidbody2D> ().gravityScale = Random.Range (0.8f, 2.2f);
			obj.transform.localEulerAngles = new Vector3 (0, 0, -45f + Random.Range (-60f, 60f));
			if (Random.Range (0, 100) > 50)
				obj.GetComponent<Rigidbody2D> ().AddTorque (-10f);
			else
				obj.GetComponent<Rigidbody2D> ().AddTorque (10f);
		} else  {
			//Time.timeScale = 1;

			randomValue = randomValue - Mathf.Min (0.5f, m_currentFishBall * 2);
			//LeanTween.delayedCall (randomValue, SpawnEnemy);

			obj.transform.localEulerAngles = new Vector3 (0, 0, -45f);
			obj.GetComponent<Rigidbody2D> ().gravityScale = Random.Range (0.8f, 2f);
			//obj.GetComponent<Rigidbody2D> ().AddTorque (Random.Range (-50f, 50));
		}


		Time.timeScale = Mathf.Clamp (0.9f + (m_currentFishBall/4f), 0.8f, 1.3f);
		//Debug.Log ("CURRENT TIME " + ( Mathf.Clamp (0.9f + (m_currentFishBall/2f), 0.8f, 1.3f) ));

		//Debug.Log ("Current Score : " + m_currentScore);
		//Debug.Log ("Current Fishball Adjuster : " + m_currentFishBall);

		m_currentFishBall += 0.02f;
		//m_currentFishBallCount++;

		//randomValue = randomValue - Mathf.Min (0.4f, m_currentFishBall);
		//randomValue = randomValue - Mathf.Min (0.8f, m_currentFishBall);
		//Debug.Log ("Random Value " + randomValue);
		//if (m_currentFishBallCount < m_currentLevel) {
			//LeanTween.delayedCall (randomValue, SpawnEnemy);
		//LeanTween.delayedCall (1.2f, SpawnEnemy);
		//}

		//LeanTween.delayedCall (randomValue, SpawnEnemy);



		float randomScale = Random.Range (0.5f, 0.8f - Mathf.Min (0.4f, m_currentFishBall / 2f));
		//float randomScale = Random.Range (0.5f, 0.8f);
		//float randomScale = Random.Range (0.4f, 0.8f);
		obj.transform.localScale = new Vector3 (randomScale, randomScale, randomScale);

		m_currentBird = 0;
		int x = 0;
		foreach (ShopBird bird in m_listShopBirds) {
		if (PlayerPrefs.GetInt ("birdbought" + x) > 0) {
		if (Random.Range (0, 100) < 30) {
		m_currentBird = x;
		}
		}
		x++;
		}
		obj.GetComponent<SpriteRenderer>().sprite = m_listShopBirds [m_currentBird].spriteImage;


		//float speedMultiplier = 1;
		//GameObject obj;
		//int randomSpawn = Random.Range (0, 100);
		//int randomSpawn = 50;
		/*if (m_currentLevel > 25 && m_currentScore <= 1) {
			obj = ZObjectMgr.Instance.Spawn2D (m_prefabBirdBig.name, m_rootSpawnLocation.transform.position);
			//speedMultiplier = 0.75f;
			speedMultiplier = 0.7f;
		}
		else if (m_currentLevel > 75 && randomSpawn > 35 && randomSpawn < 65) {
			obj = ZObjectMgr.Instance.Spawn2D (m_prefabBirdBig.name, m_rootSpawnLocation.transform.position);
			//speedMultiplier = 0.75f;
			speedMultiplier = 0.7f;
		}
		//else if (m_currentLevel > 50 && m_currentScore < m_currentLevel/2 && randomSpawn > 35 && randomSpawn < 65) {
		else if (m_currentLevel > 50 && randomSpawn > 40 && randomSpawn < 60) {
			obj = ZObjectMgr.Instance.Spawn2D (m_prefabBirdBig.name, m_rootSpawnLocation.transform.position);
			//speedMultiplier = 0.75f;
			speedMultiplier = 0.7f;
		}
		//else if (m_currentLevel > 25 && m_currentScore < m_currentLevel/2 && randomSpawn > 40 && randomSpawn < 60) {
		else */

		/*if (m_currentMode == 2) {
			m_currentLevel = m_currentScore;
		}
			
		float scaleMultipler = 1;
		if (m_currentLevel > 25 && randomSpawn > 45 && randomSpawn < 55) {
			obj = ZObjectMgr.Instance.Spawn2D (m_prefabBirdBig.name, m_rootSpawnLocation.transform.position);
			//speedMultiplier = 0.75f;
			speedMultiplier = 0.7f;
			scaleMultipler = 1.4f;
		} else {
			obj = ZObjectMgr.Instance.Spawn2D (m_prefabBird.name, m_rootSpawnLocation.transform.position);
			obj.GetComponent<Animator> ().runtimeAnimatorController = m_listShopBirds [m_currentBird].animator;
			speedMultiplier = 1;
		}



		float scaleRandom = 1f;
		if (m_currentLevel > 70) {
			obj.GetComponent<Rigidbody2D> ().velocity = new Vector2 (Random.Range (-2f, -8f) * speedMultiplier, Random.Range (-1.5f, 1.5f));
			obj.transform.position += new Vector3 (0, Random.Range (-4f, 4f), 0);
			scaleRandom = Random.Range (0.6f, 0.8f) * scaleMultipler;
		} else if (m_currentLevel > 50) {
			obj.GetComponent<Rigidbody2D> ().velocity = new Vector2 (Random.Range (-2f, -8f) * speedMultiplier, Random.Range (-1f, 1f));
			obj.transform.position += new Vector3 (0, Random.Range (-4f, 4f), 0);
			scaleRandom = Random.Range (0.6f, 0.8f) * scaleMultipler;
		} else if (m_currentLevel > 30) {
			obj.GetComponent<Rigidbody2D> ().velocity = new Vector2 (Random.Range (-2f, -7f) * speedMultiplier, Random.Range (-1f, 1f));
			obj.transform.position += new Vector3 (0, Random.Range (-4f, 4f), 0);
			scaleRandom = Random.Range (0.6f, 0.8f) * scaleMultipler;
		} else if (m_currentLevel > 10) {
			obj.GetComponent<Rigidbody2D> ().velocity = new Vector2 (Random.Range (-2f, -6f) * speedMultiplier, Random.Range (-1f, 1f));
			obj.transform.position += new Vector3 (0, Random.Range (-4f, 4f), 0);
			scaleRandom = Random.Range (0.6f, 0.8f) * scaleMultipler;
		} else if (m_currentLevel > 5) {
			obj.GetComponent<Rigidbody2D> ().velocity = new Vector2 (Random.Range (-2f, -5.4f) * speedMultiplier, 0);
			obj.transform.position += new Vector3 (0, Random.Range (-4f, 3f), 0);
			scaleRandom = Random.Range (0.7f, 0.8f) * scaleMultipler;
		} else if (m_currentLevel == 1) {
			obj.GetComponent<Rigidbody2D> ().velocity = new Vector2 (Random.Range (-2f, -2.5f) * speedMultiplier, 0);
			obj.transform.position += new Vector3(0, Random.Range(-4f, 3f), 0);
			scaleRandom = Random.Range (0.8f, 0.8f) * scaleMultipler;
		} else {
			obj.GetComponent<Rigidbody2D> ().velocity = new Vector2 (Random.Range (-2f, -4f) * speedMultiplier, 0);
			obj.transform.position += new Vector3(0, Random.Range(-4f, 1f), 0);
			scaleRandom = Random.Range (0.8f, 0.8f) * scaleMultipler;
		}

		obj.transform.localEulerAngles = Vector3.zero;
		//obj.transform.localScale = new Vector3 (-0.8f, 0.8f, 0.8f);

		obj.transform.localScale = new Vector3 (-scaleRandom, scaleRandom, scaleRandom);
		obj.GetComponent<SpriteRenderer> ().color = Color.white;
		obj.GetComponent<Bird> ().Reset ();
		obj.GetComponent<Bird> ().SetChant (m_currentBird);

		m_listBirds.Add (obj);

		m_currentEnemy = obj;*/
	}

	void RefreshMode()
	{
		if (m_currentMode == 0) {
			m_currentLevel = PlayerPrefs.GetInt ("classic");
		}
		else if (m_currentMode == 1) {
			m_currentLevel = PlayerPrefs.GetInt ("LevelQuick");
		}
			//m_currentLevel = 75;
		else if (m_currentMode == 2) {
			m_currentLevel = PlayerPrefs.GetInt ("LevelEndless");
		} else if (m_currentMode == 3) {
			m_currentLevel = PlayerPrefs.GetInt ("LevelBazooka");
		}

		ResetScore ();
	}

	void SetupPlate()
	{
		if (m_currentLevel >= 100) {
		m_textPlateTitle.text = "PLATINUM 100";
		m_imagePlate.overrideSprite = m_listPlates [3];
		} else if (m_currentLevel >= 50) {
		m_textPlateTitle.text = "GOLD 50";
		m_imagePlate.overrideSprite = m_listPlates [2];
		} else if (m_currentLevel >= 25) {
		m_textPlateTitle.text = "SILVER 25";
		m_imagePlate.overrideSprite = m_listPlates [1];
		} else {
		m_textPlateTitle.text = "";
		m_imagePlate.overrideSprite = m_listPlates [0];
		}
	}

	int m_currentBackgroundCount = 0;
	void SetupLevel()
	{
		RefreshMode ();
		ResetModeText ();

		//m_cameraMain.backgroundColor = m_listBackground[0];
		m_fixedTopBar.GetComponent<Image> ().color = m_listBackground[0];

		LeanTween.color (m_objectBackgroundObject, m_listBackground [0], 0.3f);

		//int randomColor = Random.Range (0, m_listBackground.Count);
		/*int randomColor = m_currentBackgroundCount;
		m_cameraMain.backgroundColor = m_listBackground[randomColor];
		m_fixedTopBar.GetComponent<Image> ().color = m_listBackground[randomColor];

		if (m_currentBackgroundCount >= m_listBackground.Count-1) {
			m_currentBackgroundCount = 0;
		} else {
			m_currentBackgroundCount++;
		}*/

		m_textModes.text = "BEST " + m_currentLevel;
		m_textModes2.text = "BEST " + m_currentLevel;

		m_currentRubberBandTries = PlayerPrefs.GetInt ("rubberbandtries");

		m_textLevel.color = m_colorTextSuccess;
		m_textScore.color = m_colorTextSuccess;
		m_objectReplay.SetActive (false);
		m_objectWatch.SetActive (false);
		m_objectMiss.SetActive (false);
		m_objectSuccess.SetActive (false);
		m_textHit.gameObject.SetActive (true);
		m_objectHeadshot.SetActive (false);
		m_objectParticle.SetActive (false);
		m_objectParticleExplode.SetActive(false);
		m_rootResults.SetActive (false);
		m_objectStick.SetActive (true);
		m_textTitle.gameObject.SetActive (false);
		m_objectGuide.SetActive (false);
		//m_textPlateTitle.gameObject.SetActive (true);
		m_textCharAvail.gameObject.SetActive (false);
		m_objectNewBest.SetActive (false);
		m_objectBest.SetActive (true);
		m_objectNoInternet.SetActive (false);
		m_rootUnlock.SetActive (false);


		m_objectTutorial.SetActive (true);
		m_objectTopBar.SetActive(true);



		isThrusting = false;

		SetupPlate ();

		m_currentX = 0;
		m_currentSortingLayerID = 0;

		//m_objectStick.transform.localPosition = new Vector3 (0, 0, 0);
		//m_objectStick.transform.localPosition = new Vector3 (0, 0, 0);

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		characterInstance.Reset ();
		ZObjectMgr.Instance.ResetAll ();

		foreach (GameObject btn in m_listButtons) {
			btn.gameObject.SetActive (true);
		}

		ZAnalytics.Instance.SendLevelStart ("mode" + m_currentMode, m_currentLevel, m_currentAttempts);

		m_eState = GAME_STATE.START;

		PlayerPrefs.Save ();

		UpdateBar ();
	}

	void ContinueLevel()
	{
		m_textLevel.text = "Continue";
		m_textScore.text = "" + m_currentScore;

		m_textLevel.color = m_colorTextSuccess;
		m_textScore.color = m_colorTextSuccess;
		m_objectMiss.SetActive (false);
		m_objectSuccess.SetActive (false);
		m_textHit.gameObject.SetActive (true);

		m_objectParticle.SetActive (false);

		m_objectTutorial.SetActive (true);

		ZAudioMgr.Instance.PlaySFX (m_audioButton);


		m_eState = GAME_STATE.START;
	}

	void ResetScore()
	{
		m_textLevel.text = "Level";
		//m_currentScore = m_currentLevel;
		m_currentScore = 0;
		m_textScore.text = "";// + m_currentScore;
	}

	void ShootArrow()
	{
	}
}
