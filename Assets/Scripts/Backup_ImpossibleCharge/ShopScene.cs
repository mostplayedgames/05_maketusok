﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ShopScene : ZGameScene {

	public static ShopScene instance;

	public Color m_colorBird;
	public Color m_colorNotbought;

	public AudioClip m_audioButton;

	public List<Text> m_listText;

	void Awake()
	{
		instance = this;
	}

	public void Back()
	{
		GameScene.instance.m_unlockBackgroundImage.GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
		LeanTween.alpha (GameScene.instance.m_unlockBackgroundImage, 0, 0.2f);
		this.gameObject.SetActive (false);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void SetupScene()
	{
		for (int x = 0; x < 12; x++) {
			if (PlayerPrefs.GetInt ("birdbought" + x) > 0 || x == 0) {
				GameScene.instance.m_listShopBirds [x].objectImage.SetActive (true);
				GameScene.instance.m_listShopBirds [x].imageButtons.color = m_colorBird;
				GameScene.instance.m_listShopBirds [x].objectPrice.SetActive (true);
				GameScene.instance.m_listShopBirds [x].question.SetActive (false);
			} else {
				GameScene.instance.m_listShopBirds [x].objectImage.SetActive (false);
				GameScene.instance.m_listShopBirds [x].imageButtons.color = m_colorNotbought;
				GameScene.instance.m_listShopBirds [x].objectPrice.SetActive (false);
				GameScene.instance.m_listShopBirds [x].question.SetActive (true);
			}
		}

		int y = 0;
		foreach (Text txt in m_listText) {
			txt.text = "" + GameScene.instance.m_listShopBirds [y].name ;//= GameScene.instance.GetCurrentShopPrice () + " Tusoks";
			y++;
		}
	}

	public override void OnButtonUp(ZButton button)
	{
		if (button.name == "btn_play") {
		}
	}
	
	public override void OnButtonDown(ZButton button)
	{
	}
}
